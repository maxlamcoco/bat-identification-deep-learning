# From beginning to finish process

# Before running the codes you should have ur .wav files annotated
# and the .wav and .txt should store in separate folders for easier process
import os
from shutil import rmtree
from opensoundscape import Audio, Spectrogram
from opensoundscape.annotations import BoxedAnnotations
import numpy as np
import pandas as pd
from glob import glob
from matplotlib import pyplot as plt
from opensoundscape import CNN
import torch
from glob import glob
from pathlib import Path
import pandas as pd
import random
import subprocess
from sklearn.model_selection import train_test_split
from opensoundscape.ml.cnn import use_resample_loss
from opensoundscape.preprocess.utils import show_tensor_grid, show_tensor
from opensoundscape.ml.datasets import AudioFileDataset
from opensoundscape.metrics import predict_single_target_labels
from opensoundscape.metrics import predict_multi_target_labels
from opensoundscape.ml.cnn import load_model


os.environ['MPLBACKEND'] = 'TkAgg'
plt.rcParams['figure.figsize']=[15,5] #for big visuals

# Step 1: MANIPULATING RAW .WAV AND .TXT FILE TO GENERATE A FULL DOCUMENTATION WITH ONE HOT ENCODED CSV FOR TRAINING MODEL
def import_files_as_df(annotations_files_dir, audio_files_dir):
    # Match up audio files and Raven annotations
    annotations_files = glob(f"{annotations_files_dir}/*.txt")  # Find all .txt files
    print(f"Found {len(annotations_files)} annotation files")

    audio_files = glob(f"{audio_files_dir}/*.wav")  # Find all audio files
    print(f"Found {len(audio_files)} audio files")

    # Checking if there are duplicate files for both Annotation (txt) and Sound (wav) files
    annotations_df = pd.DataFrame({'Raven_annotation_file': annotations_files})
    annotations_df.index = [Path(files).stem.split('.Table')[0] for files in annotations_files]
    print(f'\n\nRaven Annotation files with duplicate names: {annotations_df[annotations_df.index.duplicated(keep=False)]}')

    audio_df = pd.DataFrame({"Audio_file": audio_files})
    audio_df.index = [Path(files).stem for files in audio_files]
    print(f"\n\nAudio files with duplicate names: {audio_df[audio_df.index.duplicated(keep=False)]}")  # Check that there aren't duplicate audio file names

    return annotations_df, audio_df

annotations_files_dir = "D:\Selfstudy\Python\Deep Learning/BatID/Test data/Annotations/"  # Specify folder containing Raven annotations
audio_files_dir = "D:\Selfstudy\Python\Deep Learning\BatID/Test data\Bat_calls\Raven Produced/"  # Specify folder containing audio files
annotations_df, audio_df = import_files_as_df(annotations_files_dir, audio_files_dir)

# Pair up the Raven and audio files based on the audio file name
def merging_wav_txt_dfs(annotations_df, audio_df):
    paired_df = audio_df.join(annotations_df, how="outer")  # Match up Raven and audio files
    print(paired_df.to_string())

    # Check if any audio files don't have Raven annotation files
    print(f"\n\nAudio files without Raven Annotations file: {len(paired_df[paired_df.Raven_annotation_file.apply(lambda x:x!=x)])}")
    print(paired_df[paired_df.Raven_annotation_file.apply(lambda x:x!=x)])

    return paired_df

paired_df = merging_wav_txt_dfs(annotations_df, audio_df)

# SETTING UP FOR ANNOTATION SPLITTING
def one_hot_encoding(paired_df, save_path):
    boxed_annotations = BoxedAnnotations.from_raven_files(paired_df.Raven_annotation_file, paired_df.Audio_file)
    print("\n\n", boxed_annotations.df.head(3).to_string())

    # CREATE LABEL DATAFRAME (ONE HOT ENCODING)
    one_hot_df = boxed_annotations.one_hot_clip_labels(
        clip_duration=5,
        clip_overlap=0,
        min_label_overlap=0.1,
        class_subset=None,
        final_clip=None
    )
    print(one_hot_df.to_string())

    one_hot_df.reset_index().to_csv(save_path, sep=",", index=False)
    return one_hot_df

one_hot_df = one_hot_encoding(paired_df)


# # Step 2: SET MANUAL SEED
# torch.manual_seed(0)
# random.seed(0)
# np.random.seed(0)
#
# # Step 3: IMPORT TRAINING DATA AND CORRECT DATA SET TO IDEAL FORM
# one_hot_data = pd.read_csv(one_hot_df_save_path)
# one_hot_data = one_hot_data.set_index(["file", "start_time", "end_time"])[["Miniopterus australis",
#                                                                             "Scotorepens sp",
#                                                                             "Mormopterus sp2/3",
#                                                                             "Falsistrellus tasmaniensis",
#                                                                             "Rhiniolophus megaphyllus",
#                                                                             "Myotis macropus",
#                                                                             "Chalinolobus gouldii"
#                                                                         ]]
#
# # Step 4: SPLIT INTO TRAINING AND VALIDATION SETS
# train_df, validation_df = train_test_split(one_hot_data, test_size=0.2, random_state=1)
# print(train_df)
#
# # Step 4.1: CREATE MULTI-CLASS CNN MODEL
# model = CNN("resnet18", classes=train_df.columns, sample_duration=2, single_target=False)
#
# # Step 4.2: INSPECT TRAINING IMAGES
# sample_of_4 = train_df.sample(n=4)
# inspection_dataset = AudioFileDataset(sample_of_4, model.preprocessor)  # generate a dataset with the samples we wish to generate and the model's preprocessor
# inspection_dataset.bypass_augmentations = True  # Turn augmentation off for the dataset
# samples = [sample.data for sample in inspection_dataset]  # Generate the samples using the dataset
# labels = [list(sample.labels[sample.labels>0].index) for sample in inspection_dataset]
# _ = show_tensor_grid(samples,2,labels=labels)  # display the samples
# # plt.show()
#
#
# # Step 5: TRAINING THE MODEL
# # Printing and logging outputs
# model.logging_level = 2  # request lots of logged content
# model.log_file = "./Test data/Training_model/training_log.txt"
# Path(model.log_file).parent.mkdir(parents=True, exist_ok=True)
# model.verbose = 0  # Don't print anything on the screen during training
# # model.train(
# #     train_df=train_df,
# #     validation_df=validation_df,
# #     save_path="./Test data/multilabel_train/",
# #     epochs=1,
# #     batch_size=16,
# #     save_interval=5,
# #     num_workers=0
# # )
#
# # Save every 2 epochs
# model.train(
#     train_df,
#     validation_df,
#     epochs=1,
#     batch_size=64,
#     save_path='./Test data/Training_model/',
#     save_interval=2,
#     num_workers=0
# )
#
# # Save Model
# # Note model saved across diff. versions of Opensoundscape may not work (Make sure saving the model version of the model)
# model_save_path = "./Test data/Training_model/my_favorite.model"
# model_weight_path = "./Test data/Training_model/my_favorite_weights.model"
# model.save(model_save_path)
# model.save_weights(model_weight_path)
#
# # # Load Model
# # model_path = "./Test data/Training_model/my_favorite.model"
# # model = load_model(model_path)
# # model.load_weights("./Test data/Training_model/my_favorite_weights.model")  # Load weights from the state dict
#
# # # Step 5.1: GRADIENT ACTIVATION MAP
# # samples = model.generate_cams(samples=train_df.head(1))
# # samples[0].cam.plot()
#
# # # Step 5.2: PLOT THE LOSS HISTORY
# # plt.scatter(model.loss_hist.keys(), model.loss_hist.values())
# # plt.xlabel("epoch")
# # plt.ylabel("loss")
# # plt.show()
#
#
# # Step 6: PREDICTION ON DATA
# prediction_data = glob("./Test data\Prediction data/*")
# print(prediction_data)
#
# prediction_scores_df = model.predict(prediction_data)
# print(prediction_scores_df.to_string())